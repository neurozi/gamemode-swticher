# GamemodeSwitcher

## What is GamemodeSwitcher?
- GamemodeSwitcher is a project that allows you to easily switch between game modes in Minecraft. It is designed to run on servers and provides the ability to switch game modes even without operator permissions.

## Usage:
```
/switch or /gm <creative|survival|adventure|spectator>
```


## Author:
- Developed by Neurozi

## License:
- This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/).