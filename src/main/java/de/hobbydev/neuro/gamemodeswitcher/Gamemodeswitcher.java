package de.hobbydev.neuro.gamemodeswitcher;

import de.hobbydev.neuro.gamemodeswitcher.Cmd.Switch;
import org.bukkit.plugin.java.JavaPlugin;

public final class Gamemodeswitcher extends JavaPlugin {

    @Override
    public void onEnable() {
        getLogger().info("Loaded Gamemodeswitcher - By Neurozi");
        getCommand("switch").setExecutor(new Switch());

    }

    @Override
    public void onDisable() {
        getLogger().info("Unloaded Gamemodeswitcher - By Neurozi");
    }
}